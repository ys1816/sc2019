"""M345SC Homework 3, part 1
Yuriy Shulzhenko; 01194726
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.signal import hann

import scipy.sparse as sp
import scipy.linalg
import time

def nwave(alpha,beta,Nx=256,Nt=801,T=200,display=False):
    """
    Question 1.1
    Simulate nonlinear wave model

    Input:
    alpha, beta: complex model parameters
    Nx: Number of grid points in x
    Nt: Number of time steps
    T: Timespan for simulation is [0,T]
    Display: Function creates contour plot of |g| when true

    Output:
    g: Complex Nt x Nx array containing solution
    """

    #generate grid
    L = 100
    x = np.linspace(0,L,Nx+1)
    x = x[:-1]
    n=np.arange(-Nx/2,Nx/2)
    n=np.fft.fftshift(n)
    k = 2*np.pi*n/L

    def RHS(f,t,alpha,beta):
        """Computes dg/dt for model eqn.,
        f[:N] = Real(g), f[N:] = Imag(g)
        Called by odeint below
        """
        g = f[:Nx]+1j*f[Nx:]
        c= np.fft.fft(g)
        #add code here
        d2g=np.fft.ifft(-k*k*c)
        #-----------
        dgdt = alpha*d2g + g -beta*g*g*g.conj()
        df = np.zeros(2*Nx)
        df[:Nx] = dgdt.real
        df[Nx:] = dgdt.imag
        return df

    #set initial condition
    g0 = np.random.rand(Nx)*0.1*hann(Nx)
    f0=np.zeros(2*Nx)
    f0[:Nx]=g0
    t = np.linspace(0,T,Nt)

    #compute solution
    f = odeint(RHS,f0,t,args=(alpha,beta))
    g = f[:,:Nx] + 1j*f[:,Nx:]

    if display:
        plt.figure()
        plt.contour(x,t,g.real)
        plt.xlabel('x')
        plt.ylabel('t')
        plt.title('Contours of Real(g)')


    return g

def nwave2(alpha,beta,Nx=256,Nt=801,T=200,display=False):
    """
    Question 1.1
    Simulate nonlinear wave model

    Input:
    alpha, beta: complex model parameters
    Nx: Number of grid points in x
    Nt: Number of time steps
    T: Timespan for simulation is [0,T]
    Display: Function creates contour plot of |g| when true

    Output:
    g: Complex Nt x Nx array containing solution
    """

    #generate grid
    L = 100
    x = np.linspace(0,L,Nx+1)
    x = x[:-1]
    n=np.arange(-Nx/2,Nx/2)
    n=np.fft.fftshift(n)
    k = 2*np.pi*n/L

    def RHS(f,t,alpha,beta):
        """Computes dg/dt for model eqn.,
        f[:N] = Real(g), f[N:] = Imag(g)
        Called by odeint below
        """
        g = f[:Nx]+1j*f[Nx:]
        c= np.fft.fft(g)
        #add code here
        d2g=np.fft.ifft(-k*k*c)
        #-----------
        dgdt = alpha*d2g + g -beta*g*g*g.conj()
        df = np.zeros(2*Nx)
        df[:Nx] = dgdt.real
        df[Nx:] = dgdt.imag
        return df

    #set initial condition
    epsilon=0.001

    g0 = np.random.rand(Nx)*0.1*hann(Nx)
    g_new0 = g0.copy()
    g_new0[-1] += epsilon
    f0=np.zeros(2*Nx)
    f_new0=f0.copy()
    f0[:Nx]=g0
    f_new0[:Nx]=g_new0
    t = np.linspace(0,T,Nt)

    #compute solution
    f = odeint(RHS,f0,t,args=(alpha,beta))
    f_new = odeint(RHS,f_new0,t,args=(alpha,beta))
    g = f[:,:Nx] + 1j*f[:,Nx:]
    g_new = f_new[:,:Nx] + 1j*f_new[:,Nx:]

    if display:
        plt.figure()
        plt.contour(x,t,g.real)
        plt.xlabel('x')
        plt.ylabel('t')
        plt.title('Contours of Real(g)')


    return g,g_new

def analyze():
    """
    Question 1.2
    Add input/output as needed

    Discussion:

    My analysis is comprised of three main comparisons of the initial conditions: their wavenumbers,
    frequencies and stability under small changes. Let us first start with a discussion of the
    wavenumbers, and consider how looking at them can determine whether the function is chaotic for
    each initial condition.

    The first two figures depict the Fourier coefficients of the function g(x,t) restricted to the
    final time at which it was evaluated. There are 4 sets of wavenumbers in total, those at the
    initial conditions provided in the questions and, next to them, those related to the conditions
    after a small change. I also plotted figure 3 in order to restrict focus on the wavenumbers
    with the highest energy for each case. From these diagrams we can see that, although the wavenumbers
    in case A are originally less energetic (on average) than those in case B, applying a small change
    to A results in one of the wavenumbers' energy to greatly increase. On the other hand, B does not
    seem to show any similar spikes after the change, although the amount of wavenumbers which reach
    a 'high' energy does increase. Overall, it is not possible to deduce from these figures alone whether
    one case is more chaotic than the other, since both exhibit chaotic tendencies and there is no
    objective way to determine the scale at which this is the case. Plotting the highest energy wavenumber
    against time does not do much to resolve this issue (shown in figure 4).

    Having discussed the wavenumber, we can now look at the most energetic frequencies. Since the function
    g(x,t) is not periodic with respect to time, it is necessary to use the Welch function to calculate
    the energies of frequencies. From the graphs generated (in figure 5) we can see two things: firstly,
    the behaviour is very similar for cases A and B across all x. Secondly, we can determine that fmin and
    fmax (the frequencies with the minimal and maximal energies respectively) are around 0.27 and 1.78,
    (if we ignore the two-sided nature of the output which depends on the complex orientation of the
    frequencies in question). In turn, we know that the values of T and Nt that have been chosen are suitable,
    since T>>1/fmax and T/Nt < 2/fmin are both satisfied, meaning that the model can detect the fastest
    and slowest frequencies. Again, this does not help us to decide whether A or B is the more chaotic
    initial condition since the behaviour in the two cases is both regular, and similar.

    Finally, in figures 6 and 7, I considered the mean Euclidean distance as time increases between case A and
    A+epsilon, and case B and B+epsilon. Moreover, I plotted the resulting data on semilog graphs in order to
    evaluate and return the Lyapunov exponents for each case, which is a good measure of the degree of chaos
    in the system. The result is that, when taking all the wavenumbers into account, the Lyapunov constant
    for case A is greater than that for case B (0.147 vs 0.116). Although to a lesser degree, this remains
    true when restricting the analysis to the high energy wavenumbers. The conclusion is that, from the
    present analysis, the initial conditions given in case A are more chaotic than those at B.


     """
    alphaA,beta,Nx,Nt,T = 1-2j,1+2j,256,801,200
    L=100
    alphaB = 1-1j
    gA,gAE=nwave2(alphaA,beta,Nx,Nt,T)
    gB,gBE=nwave2(alphaB,beta,Nx,Nt,T)

    np.save('case_A',gA)
    np.save('case_B',gB)
    np.save('case_AE',gAE)
    np.save('case_BE',gBE)

    gA=np.load('case_A.npy')
    gB=np.load('case_B.npy')
    gAE=np.load('case_AE.npy')
    gBE=np.load('case_BE.npy')

    n=np.arange(-Nx/2,Nx/2)
    k = 2*np.pi*n/L

    cA=np.fft.fft(gA[-1])
    cA=np.fft.fftshift(cA)
    cA=abs(cA)

    cB=np.fft.fft(gB[-1])
    cB=np.fft.fftshift(cB)
    cB=abs(cB)

    cAE=np.fft.fft(gAE[-1])
    cAE=np.fft.fftshift(cAE)
    cAE=abs(cAE)

    cBE=np.fft.fft(gBE[-1])
    cBE=np.fft.fftshift(cBE)
    cBE=abs(cBE)

    fig1=plt.figure()
    plt.suptitle('Energies of different wavenumbers; Yuriy Shulzhenko; analyze()')
    plt.subplot(1,2,1)
    plt.title('Case A')
    plt.xlabel('Wavenumber')
    plt.ylabel('Energy')
    plt.plot(k,cA,'x--')

    plt.subplot(1,2,2)
    plt.title('Case A+epsilon')
    plt.xlabel('Wavenumber')
    plt.ylabel('Energy')
    plt.plot(k,cAE,'x--')
    plt.savefig('fig1.png',bbox_inches='tight')

    fig2=plt.figure()
    plt.suptitle('Energies of different wavenumbers; Yuriy Shulzhenko; analyze()')
    plt.subplot(1,2,1)
    plt.title('Case B')
    plt.xlabel('Wavenumber')
    plt.ylabel('Energy')
    plt.plot(k,cB,'x--')

    plt.subplot(1,2,2)
    plt.title('Case B+epsilon')
    plt.xlabel('Wavenumber')
    plt.ylabel('Energy')
    plt.plot(k,cBE,'x--')
    plt.savefig('fig2.png',bbox_inches='tight')

    resultA=[(k[i],cA[i],i) for i in range(len(k))]
    resultA=np.array(list(filter(lambda x: x[1]>40,resultA)))

    resultAE=[(k[i],cAE[i],i) for i in range(len(k))]
    resultAE=np.array(list(filter(lambda x: x[1]>40,resultAE)))

    resultB=[(k[i],cB[i],i) for i in range(len(k))]
    resultB=np.array(list(filter(lambda x: x[1]>40,resultB)))

    resultBE=[(k[i],cBE[i],i) for i in range(len(k))]
    resultBE=np.array(list(filter(lambda x: x[1]>40,resultBE)))


    fig3=plt.figure()
    plt.suptitle('Most energetic wavenumbers; Yuriy Shulzhenko; analyze()')
    plt.subplot(1,2,1)
    plt.title('Case A')
    plt.xlabel('Wavenumber')
    plt.ylabel('Energy')
    plt.plot(resultA[:,0],resultA[:,1],'x--', label='case A')
    plt.plot(resultAE[:,0],resultAE[:,1],'x--',label='case A+epsilon')
    plt.legend()

    plt.subplot(1,2,2)
    plt.title('Case B')
    plt.xlabel('Wavenumber')
    plt.ylabel('Energy')
    plt.plot(resultB[:,0],resultB[:,1],'x--',label='case B')
    plt.plot(resultBE[:,0],resultBE[:,1],'x--',label='case B+epsilon')
    plt.legend()
    plt.savefig('fig3.png',bbox_inches='tight')

    fig4_indexA=np.argmax(cA)
    fig4_indexB=np.argmax(cB)
    xL1=np.linspace(50,801,751)

    fig4=plt.figure()
    plt.suptitle('Behaviour of the highest energy wavenumber; Yuriy Shulzhenko; analyze()')
    plt.subplot(1,2,1)
    plt.title('Case A')
    plt.xlabel('time step')
    plt.ylabel('g(t,x)')
    plt.plot(xL1,gA[50:,fig4_indexA])

    plt.subplot(1,2,2)
    plt.title('Case B')
    plt.xlabel('time step')
    plt.ylabel('g(t,x)')
    plt.plot(xL1,gB[50:,fig4_indexB])

    plt.savefig('fig4.png',bbox_inches='tight')


    sels=np.linspace(0,256,9)
    sels=[int(x) for x in sels[:-1]]

    fig5=plt.figure()
    plt.suptitle('Energies of frequencies for different x; Yuriy Shulzhenko; analyze()')
    plt.subplot(1,2,1)
    plt.title('Case A')
    plt.xlabel('frequency')
    plt.ylabel('Pxx')
    for s in sels:
        w2,pxx2 = scipy.signal.welch(gA[:,s])
        w2=np.fft.fftshift(w2)
        pxx2=np.fft.fftshift(pxx2)
        w2=w2*Nt/T
        plt.semilogy(w2,pxx2)

    plt.subplot(1,2,2)
    plt.title('Case B')
    plt.xlabel('frequency')
    plt.ylabel('Pxx')
    max_val=0
    max_freq=0
    min_val=100
    min_freq=100
    for s in sels:
        w2,pxx2 = scipy.signal.welch(gB[:,s])
        w2=np.fft.fftshift(w2)
        pxx2=np.fft.fftshift(pxx2)
        w2=w2*Nt/T
        max=np.max(pxx2)
        if max_val<max:
            max_ind=np.argmax(pxx2)
            max_freq=w2[max_ind]
            max_val=max

        min=np.min(pxx2)
        if min_val>min:
            min_ind=np.argmin(pxx2)
            min_freq=w2[min_ind]
            min_val=min

        plt.semilogy(w2,pxx2)
    plt.savefig('fig5.png',bbox_inches='tight')


    high_energy_A=[int(x[2]) for x in resultA]
    high_energy_B=[int(x[2]) for x in resultB]



    fig6=plt.figure()
    plt.suptitle('Log plot to show the chaotic behaviour of case A; Yuriy Shulzhenko; analyze()')
    plt.subplot(1,2,1)
    plt.title('All wavenumbers')
    plt.xlabel('number of time steps')
    plt.ylabel('log of difference')
    yL1=np.sqrt(np.mean(abs(gAE[50:,:]-gA[50:,:])**2,axis=1))
    plt.semilogy(xL1,yL1)

    plt.subplot(1,2,2)
    plt.title('High energy wavenumbers')
    plt.xlabel('number of time steps')
    plt.ylabel('log of difference')
    yL1_he=np.sqrt(np.mean(abs(gAE[50:,high_energy_A]-gA[50:,high_energy_A])**2,axis=1))
    plt.semilogy(xL1,yL1_he)

    plt.savefig('fig6.png',bbox_inches='tight')
    L1=(np.log(yL1[99])-np.log(yL1[0]))/(100*T/Nt)
    L1_he=(np.log(yL1_he[99])-np.log(yL1_he[0]))/(100*T/Nt)

    fig7=plt.figure()
    plt.suptitle('Log plot to show the chaotic behaviour of case B; Yuriy Shulzhenko; analyze()')
    plt.subplot(1,2,1)
    plt.title('All wavenumbers')
    plt.xlabel('number of time steps')
    plt.ylabel('log of difference')
    yL2=np.sqrt(np.mean(abs(gBE[50:,:]-gB[50:,:])**2,axis=1))
    plt.semilogy(xL1,yL2)

    plt.subplot(1,2,2)
    plt.title('High energy wavenumbers')
    plt.xlabel('number of time steps')
    plt.ylabel('log of difference')
    yL2_he=np.sqrt(np.mean(abs(gBE[50:,high_energy_B]-gB[50:,high_energy_B])**2,axis=1))
    plt.semilogy(xL1,yL2_he)

    plt.savefig('fig7.png',bbox_inches='tight')
    L2=(np.log(yL2[99])-np.log(yL2[0]))/(100*T/Nt)
    L2_he=(np.log(yL2_he[99])-np.log(yL2_he[0]))/(100*T/Nt)


    return max_freq,min_freq,L1,L1_he,L2,L2_he#None #modify as needed

def mat_contructor(L,Nx,T):
    h_inv=Nx/L
    a,b,c=25/16,1/5,-1/80

    d0=np.zeros(Nx)
    d0[0]=-17/6
    d0[Nx-1]=17/6
    d0=h_inv*d0

    d1=np.ones(Nx-1)
    d1=d1*(a/2)
    dm1=-d1
    d1[0]=3/2
    dm1[-1]=-3/2
    d1=h_inv*d1
    dm1=h_inv*dm1

    d2=np.ones(Nx-2)
    d2=d2*(b/4)
    dm2=-d2
    d2[0]=3/2
    dm2[-1]=-3/2
    d2=h_inv*d2
    dm2=h_inv*dm2

    d3=np.ones(Nx-3)
    d3=d3*(c/6)
    dm3=-d3
    d3[0]=-1/6
    dm3[-1]=1/6
    d3=h_inv*d3
    dm3=h_inv*dm3

    dN3=np.array([0,-c/6,-c/6])*h_inv
    dmN3=np.array([c/6,c/6,0])*h_inv

    dmN2=np.array([b/4,0])*h_inv
    dN2=np.array([0,-b/4])*h_inv

    #evaluating the RHS
    B=sp.diags([dmN2,dmN3,dm3,dm2,dm1,d0,d1,d2,d3,dN3,dN2],[2-Nx,3-Nx,-3,-2,-1,0,1,2,3,Nx-3,Nx-2])
    B=sp.csr_matrix(B)

    #Now find the matrix A:
    a1=np.ones(Nx)
    a1=a1*(3/8)
    a1[1]=3
    a1[0]=0

    am1=np.ones(Nx)
    am1=am1*(3/8)
    am1[-2]=3
    am1[-1]=0

    a0=np.ones(Nx)

    A=np.array([a1,a0,am1])

    return A,B

def wavediff():
    """
    Question 1.3
    Add input/output as needed

    Discussion:

    In order to compare te finite difference and Fourier methods for finding dg/dx, I
    considered both the runtime of each method, and the difference in output (shown in
    figure 8 and figure 9 respectively). In this discussion, I first explain my approach
    to generating the figures, and then I provide my conclusion as to which method is
    optimal (at least in this scenario).

    To start off, I use nwave() to generate a large matrix g, from which I will obtain all
    the data to be used in the analysis. I do this for two reasons; firstly, nwave() is
    computationally intensive, and running it many times is highly inefficient. Secondly,
    having one set of data means that random error and variations between runtimes are
    minimised. Note that by 'large' I mean that I used a large value for Nx to generate the
    matrix.

    Having generated g, I then used equally distributed subsets of varying densities in g to
    test the two methods. By increasing the density of the subset in g, I mimic a higher value
    of Nx, and can therefore consider how the methods perform when Nx is varied. Finally, I run
    each method 10 times and take an average for each subset of g to obtain the data necessary
    to plot figure 9. The result is that, for all densities of subsets up to Nx=600, the Fourier
    method is quicker. Moreover, as we increase Nx, the method remains quicker, even though
    the increase in time is logarithmic.

    Finally, I compared the output of the two methods to take the accuracy of their solutions
    into account (since we don't have a target value of g available to us, and the method is
    not iterative, I felt this is the best approach). From figure 9 we see that the two
    solutions agree everyhwere to a high degree of accuracy, and the main difference occurs
    at the boundaries. This is expected, since the finite differences method required an
    approximation at the boundaries which would result in a slight discrepancy to the actual
    value. Since this discrepancy is <10^-5, we can accept the accuracy of both solutions
    as acceptable.

    In summary, I have found that for cases when memory usage is not a limiting factor, the
    Fourier method is optimal since it is both accurate, and faster than the finite differences
    approach. On the other hand, if we were limited by the amount of memory that is available
    to us (ie if we set Nx as very large), then the finite differences method would be the
    better approach since it has a competitive efficiency, but is also not very memory intensive,
    which means it wouldn't suffer a performance loss due to changing cache level to provide
    more memory space.

    """
    L = 100
    Nx=256
    T=100
    tot=10
    #set initial condition
    t_FD=[]
    t_Fourier=[]
    test_vals = [15,12,10,8,6,5,4,3,2,1]
    Nx_vals = [600/x for x in test_vals]
    np.save('large_g', nwave(1-1j,1+2j,Nx=600))
    large_g=np.load('large_g.npy')
    t1,t2,t3,t4=0,0,0,0

    #Now we need to solve the equation Ag'=Bg for g'. To do this, first
    #generate the matrix B which has Nx rows and cols, and also generate
    #the tridiagonal matrix A. Use scipy.sparse.

    for test in test_vals:
        Nx=600//test
        A,B=mat_constructor(L,Nx,T)
        g=large_g[:,::test]

        ############### FINITE DIFFERENCE METHOD ##################
        for j in range(tot):
            t1+=time.time()
            Bg=B.dot(g[T])
            result=scipy.linalg.solve_banded((1,1), A, Bg)
            t2+=time.time()

        ###########################################################
        t_FD+=[(t2-t1)/tot]

        n=np.arange(-Nx/2,Nx/2)
        n=np.fft.fftshift(n)
        k = 2*np.pi*n/L

        ##################### FOURIER METHOD ######################
        for j in range(tot):
            t3+=time.time()
            c= np.fft.fft(g[T])
            dg=np.fft.ifft(1j*k*c)
            t4+=time.time()
        ###########################################################
        t_Fourier+=[(t4-t3)/tot]

    plt.figure()
    plt.title('Effect of increasing Nx on runtime; Yuriy Shulzhenko; wavediff()')
    plt.xlabel('Nx')
    plt.ylabel('Runtime')
    plt.plot(Nx_vals,t_Fourier,label='Fourier')
    plt.plot(Nx_vals,t_FD,label='FD')
    plt.legend()
    plt.savefig('fig8.png',bbox_inches='tight')


    n=np.arange(-Nx/2,Nx/2)
    n=np.fft.fftshift(n)
    k = 2*np.pi*n/L
    c= np.fft.fft(large_g[T])
    dg=np.fft.ifft(1j*k*c)
    k=np.fft.fftshift(k)

    A,B=mat_constructor(L,Nx,T)
    Bg=B.dot(large_g[T])
    result=scipy.linalg.solve_banded((1,1), A, Bg)

    plt.figure()
    plt.title('Outputs of the Fourier and FD methods; Yuriy Shulzhenko; wavediff()')
    plt.xlabel('k')
    plt.ylabel('dg/dx')
    plt.plot(k,abs(dg-result))
    plt.savefig('fig9.png',bbox_inches='tight')


    return None #modify as needed

if __name__=='__main__':
    x=None
    analyze()
    wavediff()
    #Add code here to call functions above and
    #generate figures you are submitting
