"""M345SC Homework 3, part 2
Yuriy Shulzhenko; 01194726
"""
import numpy as np
import networkx as nx
import scipy.sparse as sp
import scipy.linalg as sl
from matplotlib import pyplot as plt

def growth1(G,params=(0.02,6,1,0.1,0.1),T=6):
    """
    Question 2.1
    Find maximum possible growth, G=e(t=T)/e(t=0) and corresponding initial
    condition.

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth
    y: 3N-element array containing computed initial condition where N is the
    number of nodes in G and y[:N], y[N:2*N], y[2*N:] correspond to S,I,V

    Discussion:

    My solution closely follows the information provided in lectures. Given a system
    of equations dx/dt = Mx(t), where M is a matrix acting on the vector x, we can
    find the maximal growth of x(t) by taking the exponential matrix, A=exp(Mt), and
    then using the SVD method covered in lectures to find its singular values.

    Given this method, I first construct a sparse matrix M (for better efficiency), and
    then take its exponential with the expm() function. Finally, as mentioned, I find the
    singular values and left singular vectors, and take the largest singular value and
    its corresponding singular vector. Squaring this singular value gives the growth,
    and the singular vector gives us the initial conditions.
    """
    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------
    G=0
    y = np.zeros(3*N)

    #Add code here

    ############################ Generating M ################################
    Narray_g=np.ones(N)*g
    Narray_k=np.ones(N)*k
    Narray_a=np.ones(N)*a

    dm2N=-np.ones(N)*theta

    dmN=np.zeros(2*N)
    dmN[0:N]= -dm2N

    d0=np.zeros(3*N)
    d0[0:N]= -Narray_g-Narray_k
    d0[N:2*N] = -Narray_k-Narray_a
    d0[2*N:3*N]= Narray_k

    dN=np.zeros(2*N)
    dN[0:N]=Narray_a

    Fsum=np.ones(3*N)*tau
    Fsum=sp.diags(Fsum)

    M1=sp.diags([dm2N,dmN,d0,dN],[-2*N,-N,0,N])
    M2=sp.block_diag((F,F,F))
    M=M1+M2-Fsum
    M=sp.csc_matrix(M)
    ##########################################################################

    A=sp.linalg.expm(M*T)
    u,s,vt=sp.linalg.svds(A)
    G = s[-1]**2
    y=vt[-1]

    return G,y

def growth2(G,params=(0.02,6,1,0.1,0.1),T=6):
    """
    Question 2.2
    Find maximum possible growth, G=sum(Ii^2)/e(t=0) and corresponding initial
    condition.

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth
    y: 3N-element array containing computed initial condition where N is the
    number of nodes in G and y[:N], y[N:2*N], y[2*N:] correspond to S,I,V

    Discussion:

    The method used here is very similar to that used in growth1 except now, after
    finding the exponential matrix A, we no longer require the whole matrix when
    using the SVDS method, since we only want the maximum growth of sum(I**2). To
    deal with this new requirement, we need only use the central N rows of the
    matrix A when using the SVDS function. As before, we choose the largest resulting
    singular value and its corresponding vector as the growth and initial conditions.
    """
    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------
    G=0
    y = np.zeros(3*N)

    #Add code here
    ############################ Generating M ################################
    Narray_g=np.ones(N)*g
    Narray_k=np.ones(N)*k
    Narray_a=np.ones(N)*a

    dm2N=-np.ones(N)*theta

    dmN=np.zeros(2*N)
    dmN[0:N]= -dm2N

    d0=np.zeros(3*N)
    d0[0:N]= -Narray_g-Narray_k
    d0[N:2*N] = -Narray_k-Narray_a
    d0[2*N:3*N]= Narray_k

    dN=np.zeros(2*N)
    dN[0:N]=Narray_a

    Fsum=np.ones(3*N)*tau
    Fsum=sp.diags(Fsum)

    M1=sp.diags([dm2N,dmN,d0,dN],[-2*N,-N,0,N])
    M2=sp.block_diag((F,F,F))
    M=M1+M2-Fsum
    M=sp.csc_matrix(M)
    ##########################################################################

    A=sp.linalg.expm(M*T)
    u,s,vt=sp.linalg.svds(A[N:2*N])
    G = s[-1]**2
    y=vt[-1]

    return G,y


def growth3(G,params=(2,2.8,1,1.0,0.5),T=6):
    """
    Question 2.3
    Find maximum possible growth, G=sum(Si Vi)/e(t=0)
    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth

    Discussion:

    Unlike the other two functions, growth1 and growth2, the solution to this
    problem requires a modification to the A matrix before finding the singular
    values, since otherwise there is no way we can obtain the maximal growth of
    VS via a dot product.

    With this in mind, my solution involves the use of a transformation matrix Q,
    which acts on A to give us (Vi+Si)/sqrt(2) in the top N rows, (1j*Vi)sqrt(2)
    in the next N rows, and (1j*Si)/sqrt(2) in the final N rows (note that Q is
    again a sparse matrix to ensure efficiency). The reason why I do this is because,
    when we construct the matrix QA, we have that finding the eigenavaluess of
    (QA).T QA will find the maximal growth of ((V+S)**2-V**2-S**2)/2=VS, as required.
    """
    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------
    G=0

    #Add code here

    ############################ Generating M ################################
    Narray_g=np.ones(N)*g
    Narray_k=np.ones(N)*k
    Narray_a=np.ones(N)*a

    dm2N=-np.ones(N)*theta

    dmN=np.zeros(2*N)
    dmN[0:N]= -dm2N

    d0=np.zeros(3*N)
    d0[0:N]= -Narray_g-Narray_k
    d0[N:2*N] = -Narray_k-Narray_a
    d0[2*N:3*N]= Narray_k

    dN=np.zeros(2*N)
    dN[0:N]=Narray_a

    Fsum=np.ones(3*N)*tau
    Fsum=sp.diags(Fsum)

    M1=sp.diags([dm2N,dmN,d0,dN],[-2*N,-N,0,N])
    M2=sp.block_diag((F,F,F))
    M=M1+M2-Fsum
    M=sp.csc_matrix(M)
    ##########################################################################

    A=sp.linalg.expm(M*T)

    ################### Generating the transformation matrix Q ###############
    val=np.sqrt(1/2)
    q0=np.zeros(3*N)
    qmN=np.zeros(2*N)
    q2N=val*np.ones(N)
    ivec=val*1j*np.ones(N)

    q0 = np.concatenate((q2N,np.zeros(N),ivec))
    qmN= np.concatenate((ivec,np.zeros(N)))

    Q=sp.diags([qmN,q0,q2N],[-N,0,2*N])
    Q=sp.csc_matrix(Q)
    ##########################################################################

    X=Q.dot(A)
    new=X.T.dot(X)
    evals,evecs=sp.linalg.eigs(new)
    G=max(evals.real)

    return G


def Inew(D):
    """
    Question 2.4

    Input:
    D: N x M array, each column contains I for an N-node network

    Output:
    I: N-element array, approximation to D containing "large-variance"
    behaviour

    Discussion:

    The idea of this question is to decrease dimensionality of the original data,
    while retaining as much information about the original input as possible. My
    approach to solving this problem uses PCA, which means that I first needed to
    ensure that the mean of each 'dimension' of the original data is zero. After this,
    using SVD computes the left singular vectors, and the singular values which dictate
    how much variance is associated with each singular vector. The solution is then
    computed by taking the outer product of U with A to create the transformed data,
    and then discarding rows associated with low variance. Since the singular values
    in s are in decreasing order, this is equivalent to taking the first row of the
    resulting matrix G.
    """
    N,M = D.shape
    I = np.zeros(N)

    D_M = np.mean(D,axis=0)
    D0 = D-D_M
    A=D0.T

    U,S,VT = sl.svd(A)
    G=np.dot(U.T,A)
    I=G[0]

    #Add code here
    fig=plt.figure()
    plt.plot(np.linspace(0,N,N),I)
    plt.show()


    return I


if __name__=='__main__':
    G=None
    #add/modify code here if/as desired
    #N,M = 100,5
    #G = nx.barabasi_albert_graph(N,M,seed=1)
