"""M345SC Homework 1, part 2
Yuriy Shulzhenko 01194726
"""
import random #only used in generate_L for testing nsearch.
import time
import numpy as np
import matplotlib.pyplot as plt

def search(L, target):
    """Implementation of binary search for a sorted list L to find first index
    for a value 'target', and return them as a list."""
    nA=0
    nB=len(L)-1
    target_index=-1

    while nA<=nB:
        ind = (nA+nB)//2
        if L[ind]>target:
            nB=ind-1
        elif L[ind]<target:
            nA=ind+1
        else:
            target_index=ind
            nB=ind-1

    return target_index

def generate_L(N,M,P):
    L=[]
    for i in range(N):
        arr = [random.randint(1,100) for x in range(M)]
        temp = arr[:P]
        temp.sort()
        arr[:P]=temp
        L+=[arr]
    return L



def nsearch(L,P,target):
    """Input:
    L: list containing *N* sub-lists of length M. Each sub-list
    contains M numbers (floats or ints), and the first P elements
    of each sub-list can be assumed to have been sorted in
    ascending order (assume that P<M). L[i][:p] contains the sorted elements in
    the i+1th sub-list of L
    P: The first P elements in each sub-list of L are assumed
    to be sorted in ascending order
    target: the number to be searched for in L

    Output:
    Lout: A list consisting of Q 2-element sub-lists where Q is the number of
    times target occurs in L. Each sub-list should contain 1) the index of
    the sublist of L where target was found and 2) the index within the sublist
    where the target was found. So, Lout = [[0,5],[0,6],[1,3]] indicates
    that the target can be found at L[0][5],L[0][6],L[1][3]. If target
    is not found in L, simply return an empty list (as in the code below)
    """

    Lout=[]
    nL = len(L)

    for i,arr in enumerate(L):
        sub_arr_L = arr[:P]
        ind=search(sub_arr_L,target)
        if ind!=-1:
            while(sub_arr_L[ind]==target):
                Lout+=[[i,ind]]
                ind+=1
                if ind>=P:
                    break

        sub_arr_R = arr[P:]
        for j,elt in enumerate(sub_arr_R):
            if elt==target:
                Lout+=[[i,P+j]]

    return Lout


def nsearch_time():
    """Analyze the running time of nsearch.
    Add input/output as needed, add a call to this function below to generate
    the figures you are submitting with your codes.

    Discussion:
    a)
    In my algorithm, I treat the sorted and unsorted sections separately. For the N sorted
    sublists of length P, I use a modified binary search to find the first occurence of the
    target element in each list. This is done by saving the index of the earliest target
    element at each stage, and then continuing the binary search on the elements before that
    index. Once I have found the first occurences, I add the locations of the target element
    to Lout by iterating through the lists until I reach a non-target element, or the end of
    the list. For the unsorted (M-P)*N elements, I loop through them linearly, and whenever
    one of them is equal to the target element I add its location to Lout.

    b)
    As mentioned, there are two sections to discuss; let us first deal with analysing the N
    sorted lists of length M. For these elements, we run the modified binary search for each
    list, which results in a time complexity of O(N*logP). Unfortunately, the modification
    made to find the first target element specifically means that the worst case scenario will
    always be reached, as we must always make logP iterations in every list to ensure we have
    found the first target element which appears (of course it is possible to stop the binary
    search and simply iterate backwards as we know that the target elements must all be next to
    each other in the sorted list, but since N,M,P are large, the target element will be frequent,
    and the runtime would become approximately linear). On the other hand, since we have to iterate
    through every element in the (M-P)*N unsorted elements, this is always done in linear O((M-P)N)
    time. Overall, since the leading order time complexity is of the unsorted section, and using
    the assumption that M,P,N are large, we have that the time complexity of the entire algorithm
    is O((M-P)N. Of course, as P tends to M, the runtime tends to O(Nlog(M)).

    c)
    If there is no prior information given regarding characteristics of the data, then the algorithm
    I have provided is as efficient as it can be. This is because an alternative approach would require
    further manipulation of the data such as sorting, which cannot be done in linear time (the worst case
    scenario of the fastest currently known sorting algorithm is O(NlogN) for lists of length N). Since the
    algorithm in nsearch() is actually O((M-P)N)<O(MN), we have found an algorithm which is faster than
    linear time, and so is faster than any brute-force sorting methods available. Note that the reason for
    why I discussed sorting is because we can't search an unsorted list with no prior information, while
    ensuring we have exhausted all target elements. In short, the time complexity will be at least linear
    in all possible algorithms.

    The main issue with my algorithm is that the modified binary search must have a runtime of O(logP)
    for each sublist of P elements, and has no 'best case' scenario. The reason I implemented it in
    this way was largely due to the assumption that the target element will repeat frequently, which is
    of course the case when we generate a list of random integers between a and b when the list is large.
    To be specific, the method I use is recommendable whenever logP<P/(a-b+1) since there will be, on
    average, P/(a-b+1) target elements, and a linear search through these elements to find the first
    occurence would have a greater time complexity that O(logP). When P/(a-b+1) becomes very large, it
    could also be useful to also find the final occurence of the target element, since then we would know
    every location without linearly iterating in the main bulk of the code which is inefficient.

    e)
    In my figures, I look at the effects of changing M,N and P on the runtime of nsearch(). Figures 2 and 3
    show a linear increase in runtime when either M or N is increased and everything else is kept constant.
    This is representative of my runtime discussion, where I found the time complexity to be O((M-P)N), so
    increasing either M or N will increase the runtime at the same rate (with some oscillations which are
    dependent on the frequency of the target elements).

    In figure 4, I vary the percentage of sorted elements in the N lists from 2% to 100%, which results in a
    linear decrease in runtime. This makes sense, because the greater the proportion of sorted elements, the
    closer our runtime will be to O(NlogP) since we can find more of our target elements via binary search
    rather than a brute force search through every element in the unsorted section.

    Finally, I generated figure 1 to look at how changing the proportions of P, and keeping N=M/2 will affect
    the runtime. Clearly, as we increase M (and therefore N, to retain the proportions), we have a quadratic
    increase in runtime, which is again logical from my earlier analysis. Moreover, for curves when P=0.25M,
    P=0.5M and P=0.75M, we can see that the runtime increases as the proportion of P increases, and actually
    the runtime increases at a slower rate with increasing M for high P. This is because the algorithm will be
    closer to O(NlogP) runtime, which is less than O(N(M-P)) runtime for M/P close to 1.
    """
    t_l = []
    t_m = []
    t_u = []
    x_values = [(i+1)*20 for i in range(30)]


    for i in range(30):
        time1=0
        time2=0
        time3=0
        for j in range(5):
            r=random.randint(1,100)
            L=generate_L((i+1)*10,(i+1)*20,(i+1)*5)
            t1=time.time()
            nsearch(L,(i+1)*5,r)
            t2=time.time()
            time1+=t2-t1

            L=generate_L((i+1)*10,(i+1)*20,(i+1)*10)
            t3=time.time()
            nsearch(L,(i+1)*10,r)
            t4=time.time()
            time2+=t4-t3

            L=generate_L((i+1)*10,(i+1)*20,(i+1)*15)
            t5=time.time()
            nsearch(L,(i+1)*15,r)
            t6=time.time()
            time3+=t6-t5

        t_l+=[time1/5]
        t_m+=[time2/5]
        t_u+=[time3/5]
        print(i)

    np.save('time_l',t_l)
    np.save('time_m',t_m)
    np.save('time_u',t_u)

    #more graphs

    data=np.zeros([3,50])
    xvalues=np.zeros([3,50])

    for iN in range(50):
        L=generate_L(100*(iN+1),200,100)
        target=random.randint(1,100)
        t1=time.time()
        alpha=nsearch(L,100,target)
        t2=time.time()
        data[0,iN]=t2-t1
        xvalues[0,iN]=100*(iN+1)
        print('%d completed' %(iN))

    for iM in range(50):
        L=generate_L(200,100*(iM+1),90)
        target=random.randint(1,100)
        t1=time.time()
        alpha=nsearch(L,90,target)
        t2=time.time()
        data[1,iM]=t2-t1
        xvalues[1,iM]=100*(iM+1)
        print('%d completed' %(iM))

    for iP in range(50):
        L=generate_L(1000,1000,20*(iP+1))
        target=random.randint(1,100)
        t1=time.time()
        alpha=nsearch(L,20*(iP+1),target)
        t2=time.time()
        data[2,iP]=t2-t1
        xvalues[2,iP]=20*(iP+1)
        print('%d completed' %(iP))

    np.save('time_N',data[0,:])
    np.save('time_M',data[1,:])
    np.save('time_P',data[2,:])

    data_l=np.load('time_l.npy')
    data_m=np.load('time_m.npy')
    data_u=np.load('time_u.npy')

    data_N=np.load('time_N.npy')
    data_M=np.load('time_M.npy')
    data_P=np.load('time_P.npy')

    fig=plt.figure()
    plt.plot(x_values,data_l, label='P=0.25M')
    plt.plot(x_values,data_m, label='P=0.50M')
    plt.plot(x_values,data_u, label='P=0.75M')
    plt.title('Change in search time for different M (N=M/2); Yuriy Shulzhenko; nsearch_time()')
    plt.xlabel('M value')
    plt.ylabel('time/s')
    plt.legend()
    fig.show()
    plt.savefig("fig1.png",bbox_inches='tight')

    fig=plt.figure()
    plt.plot(xvalues[0,:],data_N)
    plt.title('Change in search time for different N (M=200,P=100); Yuriy Shulzhenko; nsearch_time()')
    plt.xlabel('N value')
    plt.ylabel('time/s')
    fig.show()
    plt.savefig("fig2.png",bbox_inches='tight')

    fig=plt.figure()
    plt.plot(xvalues[1,:],data_M)
    plt.title('Change in search time for different M (N=200,P=90); Yuriy Shulzhenko; nsearch_time()')
    plt.xlabel('M value')
    plt.ylabel('time/s')
    fig.show()
    plt.savefig("fig3.png",bbox_inches='tight')

    fig=plt.figure()
    plt.plot(xvalues[2,:],data_P)
    plt.title('Change in search time for different P (N=1000,M=1000); Yuriy Shulzhenko; nsearch_time()')
    plt.xlabel('P value')
    plt.ylabel('time/s')
    fig.show()
    plt.savefig("fig4.png",bbox_inches='tight')

    return None #Modify as needed


if __name__ == '__main__':

    #add call(s) to nsearch here which generate the figure(s)
    #you are submitting
    nsearch_time() #modify as needed
