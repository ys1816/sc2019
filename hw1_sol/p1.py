"""M345SC Homework 1, part 1
Yuriy Shulzhenko 01194726
"""

def mutate_string(string,x):
    """Takes a kmer, and returns a list of its possible mutations, exluding
    the original kmer"""

    k=len(string)

    if string[x]=='A':
        mutated_strings = \
        [''.join([string[0:x],'C', string[x+1:k]]),\
        ''.join([string[0:x],'G', string[x+1:k]]),\
        ''.join([string[0:x],'T', string[x+1:k]])]

    elif string[x]=='C':
        mutated_strings = \
        [''.join([string[0:x],'A', string[x+1:k]]),\
        ''.join([string[0:x],'G', string[x+1:k]]),\
        ''.join([string[0:x],'T', string[x+1:k]])]

    elif string[x]=='G':
        mutated_strings = \
        [''.join([string[0:x],'C', string[x+1:k]]),\
        ''.join([string[0:x],'A', string[x+1:k]]),\
        ''.join([string[0:x],'T', string[x+1:k]])]

    else:
        mutated_strings = \
        [''.join([string[0:x],'C', string[x+1:k]]),\
        ''.join([string[0:x],'G', string[x+1:k]]),\
        ''.join([string[0:x],'A', string[x+1:k]])]

    return mutated_strings

def ksearch(S,k,f,x):
    """
    Search for frequently-occurring k-mers within a DNA sequence
    and find the number of point-x mutations for each frequently
    occurring k-mer.
    Input:
    S: A string consisting of A,C,G, and Ts
    k: The size of k-mer to search for (an integer)
    f: frequency parameter -- the search should identify k-mers which
    occur at least f times in S
    x: the location within each frequently-occurring k-mer where
    point-x mutations will differ.

    Output:
    L1: list containing the strings corresponding to the frequently-occurring k-mers
    L2: list containing the locations of the frequent k-mers
    L3: list containing the number of point-x mutations for each k-mer in L1.

    Discussion:
    a)
    In my algorithm, I first create a dictionary with all kmers present in the gene sequence
    as the keys, and their corresponding frequency, locations, and mutations as the values.
    After creating this dictionary, I loop through it and add all the frequent kmers to L1,
    and their locations as lists to L2. Finally, to generate L3, I developed the function
    mutate_string which takes a kmer and an index 'x' as input, and returns a list of the 3
    possible x_mutations of the kmer. Then, I loop through L1 and use this function alongside
    the original dictionary to find the amount of times the mutations of each frequent kmer
    occur, which I add to L3 iteratively.

    b)
    There are three main stages to analyze: creating the dictionary 'kmers', looping through
    it to find the frequent kmers, and looping through the frequent kmers to find their mutations.

    In 'Stage I', we have a for loop of length n-k, in which we take a substring of length k,
    check if it belongs to the dictionary (which is done in constant time), and then either
    update the dictionary if it does not appear (done in linear O(k) time), or change the value
    in the dictionary to include the location of the repeated kmer (also done in constant time).
    Overall, the worst possible time complexity of this stage is O((n-k)k), which occurs when
    every string in the sequence of length k is unique.

    Now, our analysis of 'Stage II' depends on how many unique kmers there were in the original
    gene sequence. In the worst possible case, every kmer was unique and so the for loop is of
    length n-k. In this scenario, if f is 1, then every kmer passes the following if condition,
    which means that every kmer must be added to L1. Since string concatenation is done in linear
    O(k) time, the worst possible scenario has time complexity O((n-k)k).

    Finally, in 'Stage III', we loop through all the kmers in L1 which could range from being empty
    to being of length n-k. Let us again evalutate the worst case: when the string contains n-k
    elements, we use the function mutate_string() on every element which has time complexity of
    approximately O(3k). After this, we do a search in the dictionary and update it in constant time,
    after which we update L3 in constant time. In total, the worst case scenario is O((n-k)k).

    Since the worst case scenario of each stage is O((n-k)k), we can conclude that this is also the
    time complexity of the entire algorithm, although individual scenarios will vary depending on
    how many times each kmer occurs and what we have chosen the value f to be. The main issue to be
    concerned with in this problem however is not time complexity, but rather space complexity, since
    for very large S, the dictionary method requires a lot of memory, and although the algorithm
    works quite quickly for 20mill characters and relatively small k, it struggles to have enough
    available memory when we make k large.
    """
    nS = len(S)
    kmers = {}
    L1,L2,L3=[],[],[]

    #Stage I: generate the dictionary
    for i in range(nS-k+1):
        substr = S[i:i+k]

        if kmers.get(substr) == None:
            kmers.update({substr:[1,[i],0]})
        else:
            kmers[substr][0]+=1
            kmers[substr][1]+=[i]

    #Stage II: find the frequent kmers
    for kmer in kmers:
        if kmers[kmer][0]>=f:
            L1+=[kmer]
            L2+=[kmers[kmer][1]]

    #Stage III: find the mutations of the frequent kmers
    for kmer in L1:
        mutations=mutate_string(kmer,x)
        for m in mutations: #note that there are only 3 distinct m.
            if kmers.get(m) != None:
                kmers[kmer][2]+=kmers[m][0]
        L3+=[kmers[kmer][2]]

    return L1,L2,L3




if __name__=='__main__':
    #Sample input and function call. Use/modify if/as needed
    S='CCCTATGTTTGTGTGCACGGACACGTGCTAAAGCCAAATATTTTGCTAGT'
    #k=3
    #x=2
    #f=2
    #L1,L2,L3=ksearch(S,k,f,x):
