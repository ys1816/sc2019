"""M345SC Homework 2, part 1
Yuriy Shulzhenko; 01194726
"""

#~~~~~~~~~The following function is to be used inside the function scheduler~~~~
def value_finder(L,dict,index):
    if len(L[index])==0:
        dict[index]=0
        return 0
    elif dict.get(index)==None:
        temp=max([value_finder(L,dict,i) for i in L[index]])+1
        dict[index]=temp
        return temp
    else:
        return dict[index]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def scheduler(L):
    """
    Question 1.1
    Schedule tasks using dependency list provided as input

    Input:
    L: Dependency list for tasks. L contains N sub-lists, and L[i] is a sub-list
    containing integers (the sub-list my also be empty). An integer, j, in this
    sub-list indicates that task j must be completed before task i can be started.

    Output:
    L: A list of integers corresponding to the schedule of tasks. L[i] indicates
    the day on which task i should be carried out. Days are numbered starting
    from 0.

    Discussion:

    My approach to solving this problem requires the use of a dictionary and a supplementary
    function called value_finder which updates the values in the dictionary recursively. This
    is done by looping through the lists of prerequisite tasks, L, and for each list either
    assigning the value 0 in the dictionary if the list is empty, or assigning max(tasks)+1
    for a non-empty list. This is where recursion comes into effect: if the tasks have not
    been seen before in the dictionary, then the function value_finder will work through each
    task until it reaches an empty list, or a task which is already documented. After this, it
    will move back down the stack, updating each task which had not been seen previously. This
    is efficient because when the original loop reaches this task, it will have already been
    recorded in the dictionary, so we only need to update the dictionary once per task.

    Although discussing the efficiency in full generality is quite difficult since the runtime
    largely depends on the input, I will discuss the best-case and worst-case scenarios. Clearly,
    we first have to assume that the problem is well defined (ie two tasks can't have each other
    as their prerequisites, since the function would not terminate). Assuming this is the case, the
    worst-case scenario would occur when each task has the maximum amount of prerequisites possible,
    so for N tasks there would be 0.5*N*(N+1) prerequisite 'checks' in total. Moreover, since for
    each check we either update the dictionary, search the dictionary, or move to another check (which
    involves the same process), and all these operations are done in constant time, the worst case
    complexity is O(n^2). On the other hand, if there are no prerequisite tasks, the for loop in
    scheduler will loop through once, with the value_finder function running in constant time at
    each step, hence the total time complexity is O(n) in this case (ie when M=N).
    """

    nL= len(L)
    dict={}
    S=[-1 for i in range(nL)] #Modify as needed

    for index in range(nL):
        S[index]=value_finder(L,dict,index)

    return S

def findPath(A,a0,amin,J1,J2):
    """
    Question 1.2 i)
    Search for feasible path for successful propagation of signal
    from node J1 to J2

    Input:
    A: Adjacency list for graph. A[i] is a sub-list containing two-element tuples (the
    sub-list my also be empty) of the form (j,Lij). The integer, j, indicates that there is a link
    between nodes i and j and Lij is the loss parameter for the link.

    a0: Initial amplitude of signal at node J1

    amin: If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine if the signal can successfully reach node J2 from node J1

    Output:
    L: A list of integers corresponding to a feasible path from J1 to J2.

    Discussion:

    The approach I use here is a modification of the BFS algorithm taught in lectures, which differs
    in three main ways:
        1) The algorithm terminates as soon as J2 is reached.
        2) If J2 is reached, the path which does so is remembered.
        3) Not all adjacent nodes are selected at each step, only those which satisfy a0*loss>=amin.

    In short, the way this method works is by starting with a list Q containing only J1, and popping
    the first element at each stage to check the neighbours of that element for J2, and adding them
    to the list if J2 is not found. Moreover, for every first encounter of a node (junction), we store
    it as a key in the dictionary path_dict, with its value being the node from which it was reached.
    As soon as the algorithm reaches J2, it will look up the value of the node which was adjacent to J2,
    and will repeat this process with the node value we obtain until we reach J1 to form a valid path.
    If J2 is never reached, the algorithm will keep running until it exhausts Q, and then will return
    an empty list.

    As discussed in lectures, the time complexity of the standard BFS algorithm is O(M+N) where N is the
    number of nodes, and M is the number of edges, hence it can be completed in linear time. Unlike the
    standard algorithm, I have included a 'break' condition, for when the method finds the node J2, which
    would result in a faster run time in almost all cases, except for when J2 is the final node to be found.
    Unfortunately, since this implementation depends on the position of J2 relative to J1, it does nothing
    to reduce the worst-case scenario linear run-time. On the other hand, the requirement of returning a path
    which reaches J2 from J1 explicitly results in an increase in runtime, which is at most O(N), since the
    longest possible path from J1 to J2 runs through every node in the network. This, again, has no effect
    on the time complexity. Throughout the algorithm there are several other aspects to keep track of with
    regard to time efficiency, namely adding to path_dict, or adding and removing from Q. Since searching a
    dictionary, updating the dictionary, popping and appending are all operations which can be completed in
    constant time, and since they are incorporated in the bulk of the while loop, they do not change the
    time complexity (although they do increase total runtime).
    """


    L=[] #Modify as needed
    Q=[J1]
    path_dict = {J1:-1}  #key previous node

    while(len(Q)>0 and Q[-1]!=J2):
        current_node=Q.pop()
        for elt in A[current_node]:
            if path_dict.get(elt[0])==None and a0*elt[1]>=amin:
                Q.append(elt[0])
                path_dict[elt[0]]=current_node
                if elt[0]==J2:
                    it=J2
                    while(it!=-1):
                        L=[it]+L
                        it=path_dict.get(it)
                    break

    return L


def a0min(A,amin,J1,J2):
    """
    Question 1.2 ii)
    Find minimum initial amplitude needed for signal to be able to
    successfully propagate from node J1 to J2 in network (defined by adjacency list, A)

    Input:
    A: Adjacency list for graph. A[i] is a sub-list containing two-element tuples (the
    sub-list my also be empty) of the form (j,Lij). The integer, j, indicates that there is a link
    between nodes i and j and Lij is the loss parameter for the link.

    amin: Threshold for signal boost
    If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine min(a0) needed so the signal can successfully
    reach node J2 from node J1

    Output:
    (a0min,L) a two element tuple containing:
    a0min: minimum initial amplitude needed for signal to successfully reach J2 from J1
    L: A list of integers corresponding to a feasible path from J1 to J2 with
    a0=a0min
    If no feasible path exists for any a0, return output as shown below.

    Discussion:

    To solve the problem, I modified the Dijkstra's method provided in lectures by changing the
    'optimal path' criterion. Now, instead of deciding a path's optimality by comparing its length,
    the algorithm now ensures that the largest loss on the path is minimised (ie the smallest Lij in
    the path is as large as possible). The other change I implemented is that the algorithm now also
    stores nodes in a dictionary with the values being (path loss, previous node), so that the optimal
    path can be reconstructed at the end (in the same way as in the previous question). As requested,
    if the node J2 cannot be reached for any value of a0, then an empty list is returned.

    In the way Dijkstra's is implemented here, the worst-case time complexity is O((N+M)*N), which occurs
    when all N nodes in the graph are connected. This is because, apart from iterating through the nodes,
    it is also necessary to decide which node should be checked next (via the optimality condition discussed
    earlier). In fact, this means that the code I have provided can be made more efficient by using a binary
    heap to make this decision (so that we don't have to iterate through every node). This method would
    reduce the worst-case scenario to O((N+M)logN) (where N is the number of nodes, and M is the number of
    edges), since the check can now be done in O(logN) time rather than linear time.

    Note also that the code I use is made slightly more computationally demanding, although without changing
    the worst-case scanerio, because of the path reconstruction required at the end. Since this involves
    iterating through each node in the path, and finding the next node by using Udict, the worst case is
    O(N) for this section of code.
    """

    L=[] #Modify as needed

    #Initialize dictionaries
    Edict = {} #Explored nodes
    Udict = {} #Uneplroed nodes

    for n in range(len(A)):
        Udict[n] = [0,-1] #changed from 0 to [0,-1] to also record the previous node
    Udict[J1]=[1,-1]

    #Main search
    while len(Udict)>0:
        #Find node with min d in Udict and move to Edict
        dmin = 0
        for n,w in Udict.items():
            if w[0]>dmin:
                dmin=w[0]
                nmin=n
        Edict[nmin] = Udict.pop(nmin)

        #Update provisional distances for unexplored neighbors of nmin
        for n,w in A[nmin]:
            if n in Udict:
                dcomp = min(dmin, w)
                if dcomp>Udict[n][0]:
                    Udict[n]=[dcomp,nmin]

    if Edict.get(J2)==None:
        ouput = -1,L
    else:
        it=J2
        while(it!=-1):
            L=[it]+L
            it=Edict.get(it)[1]
        output= amin/Edict.get(J2)[0], L

    return output


if __name__=='__main__':
    #add code here if/as desired
    L=None #modify as needed
