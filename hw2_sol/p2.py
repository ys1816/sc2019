"""M345SC Homework 2, part 2
Yuriy Shulzhenko; 01194726
"""
import numpy as np
import networkx as nx
from scipy.integrate import odeint
import matplotlib.pyplot as plt
import time

def model1_func(y,t,params):
    #defined a function to be used inside odeint of model1
    a,theta0,theta1,g,k,tau=params
    theta=theta0+theta1*(1-np.sin(2*np.pi*t))
    V=k*(1-y[0])-theta*y[2]*y[0]
    I=theta*y[2]*y[0]-(k+a)*y[1]
    S=a*y[1]-(g+k)*y[2]
    return V,I,S

def model1(G,x=0,params=(50,80,105,71,1,0),tf=6,Nt=400,display=False):
    """
    Question 2.1
    Simulate model with tau=0

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    S: Array containing S(t) for infected node
    """
    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1)
    S = np.zeros(Nt+1)

    y0=[0.1,0.05,0.05]

    y = odeint(model1_func,y0,tarray,args=(params,))

    #Add code here
    if display==True:
        fig=plt.figure()
        plt.plot(tarray,y[:,2])
        plt.show()


    return y[:,2]

def modelN(G,x=0,params=(50,80,105,71,1,0.01),tf=6,Nt=400,display=False,Q3=False):
    #sorry for the last parameter Q3, it makes the code much neater but I forgot to ask if I can include it!
    #I only use it for the diffusion function, and it can be ignored in the rest of the problem.
    """
    Question 2.1
    Simulate model with tau=0

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    Smean,Svar: Array containing mean and variance of S across network nodes at
                each time step.
    """
    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1)
    Smean = np.zeros(Nt+1)
    Svar = np.zeros(Nt+1)

    #~~~~~~~~~~~~~~~~~~~~~~~~~calculating the flux matrix, F ~~~~~~~~~~~~~~~~~~~
    A=nx.to_numpy_array(G)
    q=A.sum(axis=0)
    q=q.transpose()
    qA=q*A
    qA_row_sum=qA.sum(axis=0)
    qA_row_sum=1/qA_row_sum
    F=tau*qA_row_sum*qA

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    N = len(A)
    y0=np.zeros(3*N)
    y0[2*N:3*N]=1
    y0[x]=0.05
    y0[x+N]=0.05
    y0[x+2*N]=0.1

    def RHS(y,t):
        """Compute RHS of model at time t
        input: y should be a 3N x 1 array containing with
        y[:N],y[N:2*N],y[2*N:3*N] corresponding to
        S on nodes 0 to N-1, I on nodes 0 to N-1, and
        V on nodes 0 to N-1, respectively.
        output: dy: also a 3N x 1 array corresponding to dy/dt

        Discussion: add discussion here

        There are approximately 3N^2+5N+1 operations required to compute ds/dt. I evaluated this
        in the following way:
            1) F*y[0:N] requires N^2 operations, since it is matrix multiplication of a size NxN
               matrix.
            2) np.sum(,axis=?) also requires N^2 operations since we have to sum along each axis
               N times, and we do this twice so we now have 3N^2 operations.
            3) Multiplying every element of y[0:N] and y[N:2*N] by a constant takes N operations,
               so this requires another 2N operations.
            4) Adding vectors of length N takes N operations, so there are another 3N operations.
            5) g+k is one operation.
        """
        dy=np.zeros(3*N)
        theta=theta0+theta1*(1-np.sin(2*np.pi*t))
        F1,F2,F3=F*y[0:N],F*y[N:2*N],F*y[2*N:3*N]
        dy[0:N]=a*y[N:2*N]-(g+k)*y[0:N] + np.sum(F1,axis=1) - np.sum(F1,axis=0)
        dy[N:2*N]=theta*y[0:N]*y[2*N:3*N]-(k+a)*y[N:2*N] + np.sum(F2,axis=1) - np.sum(F2,axis=0)
        dy[2*N:3*N]=k*(1-y[2*N:3*N])-theta*y[0:N]*y[2*N:3*N] + np.sum(F3,axis=1) - np.sum(F3,axis=0)

        return dy


    y = odeint(RHS,y0,tarray)

    if Q3==True:
        return [y[:,0:N],y[:,N:2*N],y[:,2*N:3*N]]

    Smean=np.mean(y[:,0:N],axis=1)
    Svar=np.var(y[:,0:N],axis=1)

    if display==True:
        fig1=plt.figure()
        plt.title('Change in Smean as time increases')
        plt.xlabel('time')
        plt.ylabel('Smean')
        plt.plot(tarray,Smean)
        plt.show()

        fig2=plt.figure()
        plt.title('Change in Svar as time increases')
        plt.xlabel('time')
        plt.ylabel('Svar')
        plt.plot(tarray,Svar)
        plt.show()

    return Smean,Svar


def modelN_linear(G,x=0,params=(50,80,105,71,1,0.01),tf=6,Nt=400,display=False):

    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1)

    #calculating the Laplacian matrix
    A=nx.to_numpy_array(G)
    N = len(A)
    Q=np.zeros((N,N))
    for i in range(N):
        Q[i,i]=G.degree[i]
    L=-tau*(Q-A)

    #setting the initial conditions
    N = len(A)
    y0=np.zeros(3*N)
    y0[2*N:3*N]=1
    y0[x]=0.05
    y0[x+N]=0.05
    y0[x+2*N]=0.1

    ################ Defining the auxiliary function for odeint ################
    def RHS(y,t):

        dy=np.zeros(3*N)
        dy[0:N]= np.dot(L,y[0:N])
        dy[N:2*N]= np.dot(L,y[N:2*N])
        dy[2*N:3*N]= np.dot(L,y[2*N:3*N])

        return dy
    ############################################################################


    y = odeint(RHS,y0,tarray)

    return [y[:,0:N],y[:,N:2*N],y[:,2*N:3*N]]

def diffusion(input=(80,0.01,500,400,1)):
    """Analyze similarities and differences
    between simplified infection model and linear diffusion on
    Barabasi-Albert networks.
    Modify input and output as needed.

    Discussion: add discussion here

    Although there is a variety of possible characteristics which can be used to compare
    the two models, I decided to consider three main points: the change in mean, change in
    variance, and the way in which tau and theta affects them.

    Let us first discuss how the means of S,I,V over all nodes of the original diffusion
    model varied over time (shown in fig1,fig5 and fig9 for the standard conditions, tau=0.2
    and theta=200 respectively). In fig 1, we see how the mean of V decreases at a rate which
    is inversely proportional to I. From the equations, it is also clear that this rate of
    decrease is also linearly dependent on S, but since the number of spreaders is constant
    for the parameters chosen (this can be seen by taking a sum of the derivatives dSi/dt over
    all nodes, which sums to 0 and implies a constant value). In fact, this analysis of the
    means clearly shows the main difference between our original diffusion model and the linear
    diffusion model, since there is no exchange of V for I in the linear model. We can see this
    in figures 2,6 and 10, where the means of V,I and S are all constant over the entire graph
    (although analysis of the individual nodes would show an increase/decrease of concentration
    of V and S depending on the degree of the node).

    When looking at the variance of the two models (refer to figures 3 and 4), we can again see
    a large difference between the original model and the linear one. While the linear model has
    a simple decrease in variance as time increases (until it becomes 0), the original model shows
    an increase in variance for 'I' up to some point where it stabilises, while V and S both have a
    variance of 0 in the long term (this is expected, since S is constant, and V becomes 0 due to
    its exchange with I). We also notice that there is a peak variance in 'I', which happens while
    there is still a sizeable number of 'I' remaining, and the 'I' have spread according to node
    degree.

    Finally, I investigated the effect changing tau and theta has on the two models. Straight away,
    it is noteworthy that changing theta will clearly not have any effect on the linear diffusion
    model mean, since it only affects the rate of conversion from V to I, and this does not occur
    in the linear case. On the other hand, when we vary theta for the original model we see that the
    exchange betwwen V and I becomes quicker (linearly with respect to theta). The effect of theta
    on the variances is similar: the point of maximum variance is reached earlier, and the entire
    curve is skewed to the left. This is because the exchange between I and V happens more quickly,
    so the model does not diffuse the vulnerable cells as quickly along the network, and there ends
    up being individual nodes with greatly varying number of I cells (or V cells). Tau has a very
    similar effect on the two models, since it determines the rate at which S can spread through the
    network, and therefore affects the rate at which the infection is spread inside the greatest number
    of nodes. This will, again, have no affect on the means in the linear model since there is no
    exchange between I and V anyway, and it will again skew the variance of 'I' in the original model
    to the left since the quantities vary more quickly, and diffusion does not work to stabilise
    the corresponding concentrations.

    """

    theta0,tau,tf,Nt,no_param_change=input
    tarray = np.linspace(0,tf,Nt+1)

    G=nx.barabasi_albert_graph(100,5)
    y_original=modelN(G,params=(0,theta0,0,0,0,tau),tf=tf,Nt=Nt,Q3=True)
    y_lin_diff=modelN_linear(G,tf=tf,Nt=Nt,display=True)

    Smean=np.mean(y_original[0],axis=1)
    Svar=np.var(y_original[0],axis=1)
    Smean_lin=np.mean(y_lin_diff[0],axis=1)
    Svar_lin=np.var(y_lin_diff[0],axis=1)

    Imean=np.mean(y_original[1],axis=1)
    Ivar=np.var(y_original[1],axis=1)
    Imean_lin=np.mean(y_lin_diff[1],axis=1)
    Ivar_lin=np.var(y_lin_diff[1],axis=1)

    Vmean=np.mean(y_original[2],axis=1)
    Vvar=np.var(y_original[2],axis=1)
    Vmean_lin=np.mean(y_lin_diff[2],axis=1)
    Vvar_lin=np.var(y_lin_diff[2],axis=1)

    fig1=plt.figure()
    plt.title('Means of S,I,V for the original model; Yuriy Shulzhenko; diffusion(input='+str(input)+')')
    plt.xlabel('time/s')
    plt.ylabel('mean/%')
    plt.plot(tarray,Smean,'r',label='S')
    plt.plot(tarray,Imean,'b',label='I')
    plt.plot(tarray,Vmean,'g',label='V')
    plt.legend()
    if no_param_change==1:
        plt.savefig('fig1.png',bbox_inches="tight")
    elif no_param_change==0:
        plt.savefig('fig5.png',bbox_inches="tight")
    else:
        plt.savefig('fig9.png',bbox_inches="tight")
    plt.show()

    fig2=plt.figure()
    plt.title('Means of S,I,V for the linear diffusion model; Yuriy Shulzhenko; diffusion(input='+str(input)+')')
    plt.xlabel('time/s')
    plt.ylabel('mean/%')
    plt.plot(tarray,Smean_lin,'r',label='S')
    plt.plot(tarray,Imean_lin,'b',label='I')
    plt.plot(tarray,Vmean_lin,'g',label='V')
    plt.legend()
    if no_param_change==1:
        plt.savefig('fig2.png',bbox_inches="tight")
    elif no_param_change==0:
        plt.savefig('fig6.png',bbox_inches="tight")
    else:
        plt.savefig('fig10.png',bbox_inches="tight")
    plt.show()

    fig3=plt.figure()
    plt.title('Variances of S,I,V for the original model; Yuriy Shulzhenko; diffusion(input='+str(input)+')')
    plt.xlabel('time/s')
    plt.ylabel('mean/%')
    plt.plot(tarray,Svar,'r',label='S')
    plt.plot(tarray,Ivar,'b',label='I')
    plt.plot(tarray,Vvar,'g',label='V')
    plt.legend()
    if no_param_change==1:
        plt.savefig('fig3.png',bbox_inches="tight")
    elif no_param_change==0:
        plt.savefig('fig7.png',bbox_inches="tight")
    else:
        plt.savefig('fig11.png',bbox_inches="tight")
    plt.show()

    fig4=plt.figure()
    plt.title('Variances of S,I,V for the linear diffusion model; Yuriy Shulzhenko; diffusion(input='+str(input)+')')
    plt.xlabel('time/s')
    plt.ylabel('mean/%')
    plt.plot(tarray,Svar_lin,'r',label='S')
    plt.plot(tarray,Ivar_lin,'b',label='I')
    plt.plot(tarray,Vvar_lin,'g',label='V')
    plt.legend()
    if no_param_change==1:
        plt.savefig('fig4.png',bbox_inches="tight")
    elif no_param_change==0:
        plt.savefig('fig8.png',bbox_inches="tight")
    else:
        plt.savefig('fig12.png',bbox_inches="tight")
    plt.show()
    return None #modify as needed


if __name__=='__main__':
    #add code here to call diffusion and generate figures equivalent
    #to those you are submitting
    diffusion((80,0.01,6,400,1))
    diffusion((200,0.2,6,400,0))
    G=None #modify as needed
